import React, { Component } from 'react';
import { reposForUser } from './api';

export default class RepositoryList extends Component {
  constructor(props) {
    super(props);
    this.state = { repos: [] };
  }

  componentDidMount() {
    reposForUser('odincov').then((repos) => {
      this.setState({ repos });
    });
  }

  render() {
    return (
      <ul>
        {this.state.repos.map((repo, i) => <li key={i}>{repo.originalTitle} by {repo.author}</li>)}
      </ul>
    );
  }
}
