import 'fetch';
import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import books from './reducers/books';
import booksData from '../data/reads.json!json';
import BooksList from './containers/BooksList';

const store = createStore(books, booksData);
const rootEl = document.getElementById('app');

render(<Provider store={store}><BooksList /></Provider>, rootEl);
