export function reposForUser() {
  const url = '/data/reads.json';
  return fetch(url).then(response => response.json());
}
